package JavaAdvancedCoding;

import java.util.List;
import java.util.Scanner;

public class VendingMachine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        double amount;
        double change;
        System.out.println("Menu:");
        System.out.println("SNICKERS 0.26€ Press 1");
        double snickers = 0.26;
        System.out.println("VODKA 0.19€ Press 2");
        double vodka = 0.19;
        System.out.println("POTATO 0.33€ Press 3");
        double potato = 0.33;
        System.out.println("APPLE 0.43€ Press 4");
        double apple = 0.43;
        System.out.print("Please enter your selection: ");
        choice = scanner.nextInt();

        if (choice == 1) {
            System.out.println("Insert 0,26€ in to machine (coins: 1ct, 2ct, 5ct, 10ct, 20ct, 50ct, 1eu, 2eu)");
        } else if (choice == 2) {
            System.out.println("Insert 0,19€ in to machine (coins: 1ct, 2ct, 5ct, 10ct, 20ct, 50ct, 1eu, 2eu)");
        } else if (choice == 3) {
            System.out.println("Insert 0,33€ in to machine (coins: 1ct, 2ct, 5ct, 10ct, 20ct, 50ct, 1eu, 2eu)");
        } else if (choice == 4) {
            System.out.println("Insert 0,43€ in to machine (coins: 1ct, 2ct, 5ct, 10ct, 20ct, 50ct, 1eu, 2eu)");
        } else
            System.exit(0);


        switch (choice) {
            case 1:
                System.out.println("Specify your insert amount (eur.ct): ");
                amount = scanner.nextInt();

                System.out.println("You chose SNICKERS 0.26€");
                change = amount - snickers;
                if (change >= 0) {
                    System.out.println("Your change is: " + change);
                } else
                    System.out.println("Insert more coins");


                System.out.println("Thank you! Enjoy your purchase!");

                break;

            case 2:

                System.out.println("Specify your insert amount (eur.ct): ");
                amount = scanner.nextInt();

                System.out.println("You chose VODKA 0.19€");
                change = amount - vodka;
                if (amount>vodka) {
                    System.out.println("Your change is: " + change);
                }  else
                    System.out.println("Insert more coins");


                System.out.println("Thank you! Enjoy your purchase!");

                break;

            case 3:

                System.out.println("Specify your insert amount (eur.ct): ");
                amount = scanner.nextInt();

                System.out.println("You chose POTATO 0.33€");
                change = amount - potato;
                if (change >= 0) {
                    System.out.println("Your change is: " + change);
                } else
                    System.out.println("Insert more coins");


                System.out.println("Thank you! Enjoy your purchase!");

                break;

            case 4:

                System.out.println("Specify your insert amount (eur.ct): ");
                amount = scanner.nextInt();

                System.out.println("You chose APPLE 0.43€");
                change = amount - apple;
                if (change >= 0) {
                    System.out.println("Your change is: " + change);
                } else
                    System.out.println("Insert more coins");


                System.out.println("Thank you! Enjoy your purchase!");

                break;

            default:

                System.out.println("You must enter a selection between 1 and 4. Thank you!");

        }
    }
}

class ProductList {
    private List<Product> productStock;
    public List<Product> getProducts() {
        return productStock;
    }
    public void setProducts(List<Product> products) {
        this.productStock = products;
    }
}
class Product {
    private String name;
    private double price;
    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
class NamedEntity {
    private String name;
    private String[] properties = {};

    public NamedEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NamedEntity{" +
                "name='" + name + '\'' +
                '}';
    }
}




//Requirements
//        Implement a traditional vending machine which:
//       - Allow user to select products - the menu is displayed in console.
//       - Allow user to select what coins to insert - menu is displayed in console.
//       - Allow user to take refund by canceling the request.
//       - Return selected product and remaining change if any.
//       - The state of the vending machine is saved in a file on the disk - a json file.
//       - Vending Machine has the product menu configurable - new products can be added in the json file.
//       - Vending Machine is configurable on what coins it accepts - new coins can be added in the json file.
//        For guidance you can check the code from this repository to see how these requirements were implemented.