package JavaAdvancedCoding;

import java.time.LocalDate;

public class Person {


    public Person(String name, String lastName, String dateOfBirthday) {
    }
    public static void main(String[] args) {
    }
}
class Person1 {

    private int age;
    private String name;
    private LocalDate dob;
    public Person1(String name, String dob) {
        this.name = name;
        this.dob = LocalDate.parse(dob);
    }
    public int getAge() {
        return age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public LocalDate getDob() {
        return dob;
    }
    public void setDob(String dob) {
        this.dob = LocalDate.parse(dob);
    }
    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", dob=" + dob +
                '}';
    }
}
