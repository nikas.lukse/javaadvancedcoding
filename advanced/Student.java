package JavaAdvancedCoding;
import java.util.ArrayList;
import java.util.List;

public class Student extends Person {
    public Student(String name, String lastName, String dateOfBirthday) {
        super(name, lastName, dateOfBirthday);
        Person s1 = new Person("Jonas", "Jonaitis", "1976-02-01");
        Person s2 = new Person("Petras", "Petraitis", "1964-06-09");
        Person s3 = new Person("Saulius", "", "1957-06-15");
        Person s4 = new Person("Aiste", "Aistaviciute", "1997-07-25");
        Person s5 = new Person("Paulius", "Paulauskas", "1987-12-15");
        Person s6 = new Person("Joana", "Joanauskiene", "1982-06-12");
        Person s7 = new Person("Raimondas", "Raimondauskas", "1979-11-17");
        Person s8 = new Person("Jakaterina", "Jakaterinova", "1992-05-28");
        Person s9 = new Person("Nojus", "Nojauskas", "1997-08-09");
        Person s10 = new Person("Vita", "Vitauskaite", "1995-02-01");
        Person s11 = new Person("Stasys", "Stasiauskas", "1983-02-19");
        Person s12 = new Person("Elena", "Elenauskaite", "2003-09-15");
        Person s13 = new Person("Jone", "Jonyte", "2006-03-16");
        Person s14 = new Person("Antanas", "Antanauskas", "2010-06-02");
        Person s15 = new Person("Gina", "Ginauskaite", "2003-06-12");
    }
}
